Changelog
=========

0.0.2
-----

- Fix check-code.sh finding of .pp files
- Use Debian Bullseye

0.0.1
-----

- Puppet Checker image for checking Puppet code.
- Puppet Server image for compiling and serving Puppet manifests.
