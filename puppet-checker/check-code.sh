#!/bin/bash

set -ex

if [ -z "${CI_PROJECT_DIR}" ]; then
	echo "Error: CI_PROJECT_DIR not set."
	exit 1
elif [ ! -d "${CI_PROJECT_DIR}" ]; then
	echo "Error:CI_PROJECT_DIR=${CI_PROJECT_DIR} is not a directory."
	exit 1
fi

while read f; do
	puppet parser validate ${f}
done < <( find ${CI_PROJECT_DIR} -type f -name '*.pp' )

puppet-lint --relative ${CI_PROJECT_DIR}

find ${CI_PROJECT_DIR} -name .git -prune -o \
	\( -type f -name '*.erb' -print \) \
	| while read template ; do \
		echo "Checking ERB template $template"
		erb -x -T '-' "$template" | ruby -c
	done
