#!/bin/sh
#
# Output the first tag that points to this image or else HEAD's short commit
# ID.

set -e

if [ "${#}" -ne 0 ]; then
	echo "Usage: ${0}"
	exit 1
fi

get_image_tag() {
	tag=$( git tag --points-at HEAD | head -n 1)
	if [ -z "${tag}" ]; then
		tag=$( git rev-parse --short HEAD )
	fi
	echo ${tag}
}

get_image_tag
