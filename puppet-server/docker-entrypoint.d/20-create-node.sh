#!/bin/sh

set -ex

CODE_DIR=/etc/puppet/code/environments/production
mkdir -p ${CODE_DIR}/manifests
echo "node 'default' {  }" >> ${CODE_DIR}/manifests/nodes.pp
