Docker Images
=============

Set of Docker images used for development.

To Do
-----

- Run the server as PID 1.
- Set /usr/bin/puppet as entrypoint.
- Set an entry in /etc/hosts during startup.
